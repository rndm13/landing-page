# A basic landing page copied from figma for school project
## Features: 
- animated carousel
- animated navbar/scroll bar switching
- modal form

## How to run
```
git clone https://gitlab.com/rndm13/landing-page.gitlab
cd landing-page
npm start
```
