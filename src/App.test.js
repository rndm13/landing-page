import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders navbar", () => {
  render(<App />);
  const navBar = document.getElementById("navbar");
  expect(navBar).toBeInTheDocument();
});

test("renders button", () => {
  render(<App />);
  const button = screen.getByText("Shop now");
  expect(button).toBeInTheDocument();
});
