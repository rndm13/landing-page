import "./App.css";

import { useState } from "react";

import { ArrowLeft, ArrowRight, Cross } from "./Icons";

const Modal = ({ children, setOpen }) => {
  return (
    <>
      <div className="modal-back"></div>
      <div className="modal-wrapper">
        <div className="modal" id="modal">
          <div className="pin-right">
            <button className="modal-cross" onClick={() => setOpen(false)}>
              <Cross />
            </button>
          </div>
          {children}
        </div>
      </div>
    </>
  );
};

const LoginForm = () => {
  return (
    <form action="" className="form">
      <h1>Login</h1>
      <div className="input-group">
        <label for="name">Name:</label>
        <input type="text" name="name" className="text-input" />
      </div>
      <div className="input-group">
        <label for="password">Password:</label>
        <input type="password" name="password" className="text-input" />
      </div>
      <button className="primary-button">Log in</button>
    </form>
  );
};

const Authentication = () => {
  let [open, setOpen] = useState(false);

  return (
    <>
      <button
        className="icon-button"
        onClick={() => {
          setOpen(true);
          console.log(open);
        }}
      >
        <img src="profile.svg" alt="Profile icon" />
      </button>
      {open ? (
        <Modal setOpen={setOpen}>
          <LoginForm />
        </Modal>
      ) : (
        <> </>
      )}
    </>
  );
};

const Header = () => {
  let [searchActive, setSearchActive] = useState(false);

  return (
    <header className="navbar" id="navbar">
      <img src="logo.svg" alt="Site logo" className="navbar-logo" />
      <nav className="navbar-center">
        <div
          className={"navbar-links " + (searchActive ? "hide-left-shrink" : "")}
        >
          <a href="/">About</a>
          <a href="/">Features</a>
          <a href="/">Pricing</a>
          <a href="/">Gallery</a>
          <a href="/">Team</a>
        </div>
        <form
          className={
            "search-bar-form " + (searchActive ? "" : "hide-right-shrink")
          }
        >
          <input
            type="text"
            name="search"
            className="search-bar-input"
            placeholder="Search..."
          />
        </form>
      </nav>
      <div className="navbar-icons">
        <button
          className="icon-button"
          onClick={() =>
            setSearchActive((old) => {
              console.log(!old);
              return !old;
            })
          }
        >
          <img src="search.svg" alt="Search icon" />
        </button>
        <button className="icon-button">
          <img src="cart.svg" alt="Cart icon" />
        </button>
        <Authentication />
      </div>
    </header>
  );
};
const Landing = () => {
  const first_slide = (
    <>
      <div className="legend-content">
        <div className="legend-title">{"Women's fashion"}</div>
        <div className="legend-description">
          We offer the best materials, unique cut and modern clothing
          collections. Despite the fact that we are quite young, the clothes
          created under our leadership are already very popular all over the
          world.
        </div>
        <button className="primary-button">Shop now</button>
      </div>
      <div className="legend-side">
        <div className="sale-container">
          <img src="/carousel-item-1.png" alt="Woman with transparent bag" />
          <div className="sale-circle"> -50% </div>
        </div>
      </div>
    </>
  );
  const second_slide = (
    <>
      <div className="legend-content">
        <div className="legend-title">Lorem ipsum</div>
        <div className="legend-description">
          Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit
          enim labore culpa sint ad nisi Lorem pariatur mollit ex esse
          exercitation amet. Nisi anim cupidatat excepteur officia.
          Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate
          voluptate dolor minim nulla est proident. Nostrud officia pariatur ut
          officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit
          commodo officia dolor Lorem duis laboris cupidatat officia voluptate.
          Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis
          officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis
          sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea
          consectetur et est culpa et culpa duis.
        </div>
        <button className="primary-button">Shop now</button>
      </div>
      <div className="legend-side">
        <div className="sale-container">
          <img src="/carousel-item-2.png" alt="Woman with big skirt" />
          <div className="sale-circle"> -30% </div>
        </div>
      </div>
    </>
  );
  return (
    <div className="body">
      <Header />
      <main className="landing-main">
        <div className="legend">
          <Carousel
            items={[
              first_slide,
              second_slide,
              first_slide,
              first_slide,
              second_slide,
              first_slide,
              first_slide,
              second_slide,
              second_slide,
            ]}
          />
        </div>
      </main>
    </div>
  );
};

const Carousel = ({ items }) => {
  let [currentItemId, setCurrentItemId] = useState(0);
  return (
    <div className="carousel no-scroll">
      {items.map((el, i) => {
        let className = "";
        if (i < currentItemId) {
          className = "hide-left";
        }
        if (i > currentItemId) {
          className = "hide-right";
        }
        return (
          <div className={"carousel-item " + className} key={i}>
            {el}
          </div>
        );
      })}
      <div className="carousel-buttons">
        <button
          className="icon-button"
          onClick={() => {
            setCurrentItemId((old) => (old === 0 ? old : old - 1));
          }}
        >
          <ArrowLeft />
        </button>
        <button
          className="icon-button"
          onClick={() => {
            setCurrentItemId((old) =>
              old === items.length - 1 ? old : old + 1,
            );
          }}
        >
          <ArrowRight />
        </button>
      </div>
    </div>
  );
};

const App = () => {
  return <Landing />;
};

export default App;
